### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 0e219f90-d82b-11eb-301c-b14da08c8325
using PlutoUI

# ╔═╡ adee7acf-70d7-4374-97dc-5209f43d0350
md"## Capturing the Sequential Game"

# ╔═╡ b8dbe890-d813-11eb-24ea-53f6b7614dc2
 payoffMatrix =[1 -1 -2 2; -3 3 0 0]

# ╔═╡ 8243ef47-b633-41a4-bc82-9dae73960b9e
md"## Calculating Probability"

# ╔═╡ ec637e83-02d5-4048-bfc4-bcca0ffcdb4c
function probability(matrix)
	# player 1  utility
	leftP = (matrix[3]) + 
			((-1) * (matrix[4])) + 
			((-1) * (matrix[7] + ((-1) * (matrix[8]))))
	rightP = ((-1) * (matrix[4])) + (matrix[8])
	p = rightP//leftP
	
	#player 2 utility
	leftQ = (matrix[1]) + 
			((-1) * (matrix[5])) + 
			((-1) * (matrix[2] + ((-1) * (matrix[6]))))
	rightQ = ((-1) * (matrix[5])) + (matrix[6])
	q = rightQ//leftQ
	
	pq =Dict("p" => p, 
			 "q" => q
			)
	return pq
	
end

# ╔═╡ 93e763ff-937f-45c7-9e51-66ba9ccb76d6
md"## Calculating the Utility of the Payoff"

# ╔═╡ 72e215f4-5662-4982-a47f-ca45a360a481
function player1_utility(matrix)
	
	eq = ((probability(payoffMatrix)["p"]) * (probability(payoffMatrix)["q"]) * matrix[1]) +
	((probability(payoffMatrix)["p"]) * (1-probability(payoffMatrix)["q"]) * matrix[5]) +
	((1-probability(payoffMatrix)["p"]) * probability(payoffMatrix)["q"] * matrix[2]) +
	((1-probability(payoffMatrix)["p"]) * (1-probability(payoffMatrix)["q"]) * matrix[6])
	
end

# ╔═╡ 6768978f-fae2-4390-b165-a8c5c1d252cf
function player2_utility(matrix)
	eq = ((probability(payoffMatrix)["p"]) * (probability(payoffMatrix)["q"]) * matrix[3]) +
	((probability(payoffMatrix)["p"]) * (1-probability(payoffMatrix)["q"]) * matrix[7]) +
	((1-probability(payoffMatrix)["p"]) * probability(payoffMatrix)["q"] * matrix[4]) +
	((1-probability(payoffMatrix)["p"]) * (1-probability(payoffMatrix)["q"]) * matrix[8])
	return eq
end

# ╔═╡ ec63463b-bf0b-489d-a22f-e0de0f1906e2
with_terminal() do
	println("Player 1 Payoff: $(player1_utility(payoffMatrix))
		\nPlayer 2 Payoff: $(player2_utility(payoffMatrix))")
end

# ╔═╡ Cell order:
# ╠═0e219f90-d82b-11eb-301c-b14da08c8325
# ╟─adee7acf-70d7-4374-97dc-5209f43d0350
# ╠═b8dbe890-d813-11eb-24ea-53f6b7614dc2
# ╟─8243ef47-b633-41a4-bc82-9dae73960b9e
# ╠═ec637e83-02d5-4048-bfc4-bcca0ffcdb4c
# ╟─93e763ff-937f-45c7-9e51-66ba9ccb76d6
# ╠═72e215f4-5662-4982-a47f-ca45a360a481
# ╠═6768978f-fae2-4390-b165-a8c5c1d252cf
# ╠═ec63463b-bf0b-489d-a22f-e0de0f1906e2
