### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 899f4dd0-d618-11eb-3123-3fcdadeb0b88
 using Random


# ╔═╡ 918938a0-d602-11eb-278e-9344f05ae277
struct MDP
    initial
    states
    actions
    terminal_states
    transitions
    gamma::Float64
    rewards 
    
    function MDP(initial,actions, terminal_states , transitions ,states, discount ,reward)
        return new(initial, states, actions, terminal_states, transitions, discount,reward);
    end
end


# ╔═╡ c95d36a0-d602-11eb-26d4-033bdced1164
#Reward function for returning the reward
function  reward(mdp , state)
    print("\nrewards :",mdp.rewards,"\n")
    print("\nReward and state :",state ,"\n")
    print("\nReward  :",mdp.rewards[state] , "\n")
    return mdp.rewards[state];
end


# ╔═╡ d1ff4be0-d602-11eb-35c5-4d9c799e4639
#transion model to return the transition_model of the state with the given action
function transition_model(mdp, state ,action)

    return [mdp.transitions[state][action]];
end



# ╔═╡ 34c5f740-d740-11eb-32da-43a1101964c9
function argmax(seq::T, fn) where {T <: Vector}
     be = seq[1];
     bs = fn(be);
    for e in seq
        element_score = fn(e);
        if (element_score > bs)
            be = element;
            bs = element_score;
        end
    end
    return be;
end


# ╔═╡ e3c131e0-d602-11eb-2561-7b9a2f0b4738

#actions function to return the actions
function actions(mdp , state)
    
        print("\nActions :",mdp.actions)
        return mdp.actions;
   
end



# ╔═╡ f4447d60-d602-11eb-1a68-5d96cd649c6a
# expected utility function returns the expected utility of doing action in state s
function expected_utility(mdp , U , state , action)
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, action)));
end


# ╔═╡ eb2a6c70-d617-11eb-24b0-a195e54239b0
function policy_evaluation(pi::Dict, U::Dict, mdp; k::Int64=10)
    for i in 1:k
        for state in mdp.states
            U[state] = (reward(mdp, state)
                        + (mdp.gamma
                        * sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, pi[state])))));
        end
    end
    return U;
end

# ╔═╡ 09b17f1e-d605-11eb-286a-11a14e479a01
function policy_iteration(mdp)
     U::Dict = Dict(collect(Pair(state, 0.0) for state in mdp.states));
	#randomise states
    pi::Dict = Dict(collect(Pair(state, rand(collect(actions(mdp, state))))
                                    for state in mdp.states));
	
	
    while (true)
        U = policy_evaluation(pi, U, mdp);
        local unchanged::Bool = true;
        for state in mdp.states
			#Policy Improvement
            local action = argmax(collect(actions(mdp, state)), (function(action)
                                                                    return expected_utility(mdp, U, state, action);
                                                                 end));
            if (action != pi[state])
                pi[state] = action;
                unchanged = false;
            end
        end
        if (unchanged)
            return pi;
         end
     end
end

# ╔═╡ 06b4a4c0-d603-11eb-19da-5ff556e46bbe
tm = Dict([Pair("S1", Dict([Pair("UP", (0.7, "S5")), Pair("RIGHT", (0.1, "S2")),Pair("DOWN", (0, "S1")),Pair("LEFT", (0, "S1"))])),
            Pair("S2", Dict([Pair("UP", (0.7, "S6")), Pair("RIGHT", (0.1, "S3")),Pair("LEFT", (0.5, "S1")),Pair("DOWN", (0, "S2"))])),
            Pair("S3", Dict([Pair("UP", (0.7, "S7")), Pair("RIGHT", (0.1, "S4")),Pair("LEFT", (0.1, "S2")),Pair("DOWN", (0, "S3"))])),
		    Pair("S4", Dict([Pair("UP", (0.7, "S8")), Pair("LEFT", (0.1, "S3")),Pair("RIGHT", (0, "S4")),Pair("DOWN", (0, "S4"))])),
            Pair("S5", Dict([Pair("UP", (0.7, "S9")), Pair("RIGHT", (0.1, "S6")),Pair("DOWN", (0.1, "S1")),Pair("LEFT", (0, "S5"))])),
           Pair("S6", Dict([Pair("DOWN", (0, "S3")), Pair("RIGHT", (0, "S8")),Pair("UP", (0, "S11")),Pair("LEFT", (0, "S7"))])),
		    Pair("S7", Dict([Pair("DOWN", (0.1, "S3")), Pair("RIGHT", (0.1, "S8")),Pair("UP", (0.7, "S11")),Pair("LEFT", (0, "S7"))])),
            Pair("S8", Dict([Pair("UP", (0.7, "S12")), Pair("LEFT", (0.1, "S7")),Pair("DOWN", (0.1, "S4")),Pair("RIGHT", (0, "S8"))])),
            Pair("S9", Dict([Pair("DOWN", (0.1, "S5")), Pair("RIGHT", (0.1, "S10")),Pair("LEFT", (0, "S9")),Pair("UP", (0, "S9"))])),
		    Pair("S10",  Dict([Pair("DOWN", (0.1, "S6")), Pair("RIGHT", (0.1, "S11")),Pair("LEFT", (0.1, "S9")),Pair("UP", (0, "S10"))])),
            Pair("S11",  Dict([Pair("DOWN", (0.1, "S7")), Pair("LEFT", (0.1, "S10")),Pair("RIGHT", (0.1, "S12")),Pair("UP", (0, "S11"))])),
            Pair("S12", Dict([Pair("LEFT", (0, "S11")), Pair("DOWN", (0, "S8")),Pair("UP", (0, "S12")),Pair("RIGHT", (0, "S12"))]))
		  
		
		
		
		
		]);


# ╔═╡ 8277fe40-d603-11eb-3e3e-077c9b2420aa
rewards = Dict("S1" => 0 , "S2" => 0, "S3" => 0,"S4" => 0 , "S5" => 0, "S6" => 0,"S7" => 0 , "S8" => -1, "S9" => 0,"S10" => 0 , "S11" => 0, "S12" => 1)

# ╔═╡ 1730eb60-d603-11eb-0a1a-71225e1024e4
mdp = MDP("S1", Set(["UP", "DOWN","LEFT","RIGHT"]), Set(["S12"]), tm, Set(["S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12"]),0.1,rewards);


# ╔═╡ e7f839ee-d604-11eb-2d96-8300ae218651
optimal = policy_iteration(mdp)

# ╔═╡ e141c810-d604-11eb-1aba-110d7c817d3d
tinashe = [optimal["S9"]  optimal["S10"] optimal["S11"] optimal["S12"] ;optimal["S5"] nothing optimal["S7"] optimal["S8"];optimal["S1"] optimal["S2"] optimal["S3"] optimal["S4"]]

# ╔═╡ Cell order:
# ╠═899f4dd0-d618-11eb-3123-3fcdadeb0b88
# ╠═918938a0-d602-11eb-278e-9344f05ae277
# ╠═c95d36a0-d602-11eb-26d4-033bdced1164
# ╠═d1ff4be0-d602-11eb-35c5-4d9c799e4639
# ╠═34c5f740-d740-11eb-32da-43a1101964c9
# ╠═e3c131e0-d602-11eb-2561-7b9a2f0b4738
# ╠═f4447d60-d602-11eb-1a68-5d96cd649c6a
# ╠═eb2a6c70-d617-11eb-24b0-a195e54239b0
# ╠═09b17f1e-d605-11eb-286a-11a14e479a01
# ╠═06b4a4c0-d603-11eb-19da-5ff556e46bbe
# ╠═8277fe40-d603-11eb-3e3e-077c9b2420aa
# ╠═1730eb60-d603-11eb-0a1a-71225e1024e4
# ╠═e7f839ee-d604-11eb-2d96-8300ae218651
# ╠═e141c810-d604-11eb-1aba-110d7c817d3d
